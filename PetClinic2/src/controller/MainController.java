package controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import model.Animal;
import util.DatabaseUtil;

public class MainController implements Initializable {
	@FXML
	 private ListView<String> mainListView;
	@FXML
	 private ListView<String> listView;
	@FXML
	 private Button addAppointment;
	@FXML
	void addAppoinmentClicked(ActionEvent e) {
		System.out.println("test");
	}

	public void populateMainListView() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Animal> animalDBList = (List<Animal>)db.animalList();
		ObservableList<Animal> animalList = FXCollections.observableArrayList(animalDBList);
		mainListView.setItems(getAnimalNameObservableList(animalList));
		mainListView.refresh();
		db.closeEntityManager();
	}
	
	public ObservableList<String> getAnimalNameObservableList(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a: animals) {
			names.add(a.getName());
		}
		return names;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			populateMainListView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
