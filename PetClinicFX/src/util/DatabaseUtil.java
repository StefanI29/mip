package util;

import java.sql.Date;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Personalmedical;
import model.Programare;

public class DatabaseUtil {

	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	/**
	 * @throws Exception
	 */
	public void setUp()  {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetClinicFX");
		entityManager = entityManagerFactory.createEntityManager();
	}

	/**
	 * @param animal
	 */
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}

	/**
	 * The save function of medical staff
	 * 
	 * @param persMed
	 */
	public void savePersonalMedical(Personalmedical personalMedical) {
		entityManager.persist(personalMedical);
	}

	/**
	 * The save function of appointment
	 * 
	 * @param prog
	 */
	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
	}

	/**
	 * Hear we delete an animal by a parameter which is type animal
	 * 
	 * @param animal
	 */
	public void deleteAnimal(Animal animal) {
		entityManager.remove(animal);
	}

	/**
	 * Hear we delete a medical personnel by a parameter which is type
	 * Personalmedical
	 * 
	 * @param persMed
	 */
	public void deletePersonalMedical(Personalmedical personalMedical) {
		entityManager.remove(personalMedical);
	}

	/**
	 * Hear we delete an appointment by a parameter which is type Programare
	 * 
	 * @param prog
	 */
	public void deleteProgramare(Programare programare) {
		entityManager.remove(programare);
	}

	/**
	 * Hear we delete an animal by a parameter which is id of an animal
	 * 
	 * @param animal
	 */
	public void deleteAnimalById(int animal) {
		Animal result = entityManager.find(Animal.class, animal);
		entityManager.remove(result);
	}

	/**
	 * Hear we delete a medical personnel by a parameter which is id of a medical
	 * personnel
	 * 
	 * @param persMed
	 */
	public void deletePersonalMedicalById(int personalMedical) {
		Personalmedical result = entityManager.find(Personalmedical.class, personalMedical);
		entityManager.remove(result);
	}

	/**
	 * Hear we delete an appointment by a parameter which is id of an appointment
	 * 
	 * @param prog
	 */
	public void deleteProgramareById(int prog) {
		Programare result = entityManager.find(Programare.class, prog);
		entityManager.remove(result);
	}

	/**
	 * Update an animal with a new name and type and we give the id of animal that
	 * we want to modify
	 * 
	 * @param animal
	 * @param newName
	 * @param newTipAnimal
	 */
	public void updateAnimalById(int animal, String newName, String newTipAnimal) {
		Animal anim = entityManager.find(Animal.class, animal);
		// anim.setIdAnimal(newId);
		anim.setName(newName);
		anim.setTipAnimal(newTipAnimal);
		// entityManager.merge(anim);
	}

	/**
	 * Update a medical personnel with a new name and type and we give the id of
	 * medical personnel that we want to modify
	 * 
	 * @param persMed
	 * @param newName
	 * @param newTip
	 */
	public void updatePersonalMedicalById(int persMed, String newName, String newTip) {
		Personalmedical anim = entityManager.find(Personalmedical.class, persMed);
		anim.setName(newName);
		anim.setTipPersonalMedical(newTip);
	}

	/**
	 * Update an appointment with a new id for animal, a new id for medical
	 * personnel and a new data and we give the id of appointment that we want to
	 * modify
	 * 
	 * @param prog
	 * @param newIdAnimal
	 * @param newIdPErsMedical
	 * @param newData
	 */
	public void updateProgramareById(int programare, int newIdAnimal, int newIdPersonalMedical, Date newData) {
		Programare result = entityManager.find(Programare.class, programare);
		result.setData(newData);

	}

	public void beginTransaction() {
		entityManager.getTransaction().begin();
	}

	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}

	public void closeEntityManager() {
		entityManager.close();
	}

	/**
	 * Print all animals form database
	 */
	public void printAllAnimalsFormBD() {
		List<Animal> results = entityManager.createNativeQuery("Select * from PetShop.Animal", Animal.class)
				.getResultList();
		for (Animal animal : results) {
			System.out.println("Animal : " + animal.getName() + " has ID: " + animal.getIdAnimal());

		} // Perogramare programare = new Programare();
			// programare.delete();
	}

	/**
	 * Print all medical personnel from database
	 */
	public void printAllPersMedicalFormBD() {
		List<Personalmedical> results = entityManager
				.createNativeQuery("Select * from PetShop.Personalmedical", Personalmedical.class)
				.getResultList();
		for (Personalmedical persMed : results) {
			System.out.println("Personal medical nume : " + persMed.getName() + " has ID: "
					+ persMed.getIdPersonalMedical() + " tipul: " + persMed.getTipPersonalMedical());

		}
	}

	/*
	 * Print all appointments from database
	 */
	public void printAllProgramariFormBD() {
		Animal animal = new Animal();
		Personalmedical personalMedical = new Personalmedical();
		List<Programare> results = entityManager
				.createNativeQuery("Select * from PetShop.Programare", Programare.class).getResultList();
		for (Programare prog : results) {
			System.out.println("ID Programare : " + prog.getIdprogramare() + " ID animal: " + animal.getIdAnimal()
					+ " ID Personal Medical " + personalMedical.getIdPersonalMedical() + " Data" + prog.getData());

		}
	}

	/**
	 * Sort all appointments from database by data and print the id of appointment,
	 * the name of the animal and the name of the medical staff
	 */
	public void sortAllProgramariFormBD() {
		List<Programare> results = entityManager
				.createNativeQuery("Select * from cabinetveterinar.Programare", Programare.class).getResultList();
		results.sort(Comparator.comparing(Programare::getData));
		for (Programare prog : results) {
			System.out.println("ID Programare : " + prog.getIdprogramare() + " nume animal: "
					+ entityManager.find(Animal.class, prog.getAnimal()).getName() + " nume Personal Medical "
					+ entityManager.find(Personalmedical.class, prog.getPersonalmedical()).getName() + " Data"
					+ prog.getData());

		}
	}
	
	public List<Animal> animalList() {
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a FROM Animal a",Animal.class).getResultList();
		return animalList;
	}
}
