package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idprogramare;

	@Temporal(TemporalType.DATE)
	private Date data;

	private String tipProgramare;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idAnimal")
	private Animal animal;

	//bi-directional many-to-one association to Personalmedical
	@ManyToOne
	@JoinColumn(name="idPersonalMedical")
	private Personalmedical personalmedical;

	public Programare() {
	}

	public int getIdprogramare() {
		return this.idprogramare;
	}

	public void setIdprogramare(int idprogramare) {
		this.idprogramare = idprogramare;
	}

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getTipProgramare() {
		return this.tipProgramare;
	}

	public void setTipProgramare(String tipProgramare) {
		this.tipProgramare = tipProgramare;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Personalmedical getPersonalmedical() {
		return this.personalmedical;
	}

	public void setPersonalmedical(Personalmedical personalmedical) {
		this.personalmedical = personalmedical;
	}

}