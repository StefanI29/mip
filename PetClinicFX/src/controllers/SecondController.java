package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SecondController {

	@FXML
	private Button close;
	
	@FXML
	private void closeButtonAction(){
	    Stage stage = (Stage) close.getScene().getWindow();
	    stage.close();
	}
	
	@FXML
	private Label label1;
	
	@FXML
	private Label label2;
	
	@FXML
	private TextField t1;
	
	@FXML
	private TextField t2;
	
}
