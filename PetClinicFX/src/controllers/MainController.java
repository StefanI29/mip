package controllers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Animal;
import util.DatabaseUtil;

public class MainController implements Initializable {

	@FXML
	private ListView<String> listView;

	@FXML
	private Button closeButton;

	@FXML
	private ImageView imageView;

	@FXML
	private Button saveButton;

	@FXML
	private void closeButtonAction() {
		Stage stage = (Stage) closeButton.getScene().getWindow();
		stage.close();
	}

	@FXML
	private void saveButton() throws Exception {

		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(getClass().getResource("Diagnostic.fxml"));
		Scene scene = new Scene(fxmlLoader.load(), 600, 400);
		Stage stage = new Stage();
		stage.setTitle("New Window");
		stage.setScene(scene);
		stage.show();
	}

	public void populateMainListView() throws FileNotFoundException {
		DatabaseUtil databaseUtil = new DatabaseUtil();
		databaseUtil.setUp();
		databaseUtil.beginTransaction();
		List<Animal> animalDBList = (List<Animal>) databaseUtil.animalList();
		ObservableList<String> animalNamesList = getAnimalName(animalDBList);
		// Creating an image
		Image image = new Image(new FileInputStream("C:\\Proiecte Java\\dog.jpeg"));

		// Setting the image view
		imageView = new ImageView(image);
		// Setting the position of the image

		imageView.setX(50);
		imageView.setY(25);

		// setting the fit height and width of the image view
		imageView.setFitHeight(455);
		imageView.setFitWidth(500);

		// Setting the preserve ratio of the image view
		imageView.setPreserveRatio(true);
		listView.setItems(animalNamesList);
		listView.refresh();
		databaseUtil.closeEntityManager();
	}

	public ObservableList<String> getAnimalName(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a : animals) {
			names.add(a.getName());
		}
		return names;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			populateMainListView();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
